//
//  ViewController.swift
//  singleton-lista-contactos
//
//  Created by Developer on 12/07/2018.
//  Copyright © 2018 marinateixeira. All rights reserved.
//

import UIKit

class ViewController:UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    
    var adr : UIImage!
    
    @IBOutlet weak var _img: UIImageView!
    @IBOutlet weak var _nome: UITextField!
    @IBOutlet weak var _sobrenome: UITextField!
    @IBOutlet weak var _tel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func OpenCamera(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.present(imagePicker,animated: false,completion: nil)
            
        }
        
        
    }
    
    
    @IBAction func OpenBiblioteca(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker,animated: false,completion: nil)
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            adr = image
            _img.image = image
            _img.layer.cornerRadius = _img.frame.height/2
            _img.clipsToBounds = true
        }
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func _Btn(_ sender: Any) {
        var adx = 0
        
        //_nome.text = ""
        //_sobrenome.text = ""
        //_tel.text = ""
        
        Singleton.shared.append(Singleton(Nome : _nome.text!  , Sobrenome : _sobrenome.text! , Tel : _tel.text! , Image : adr))
        
        Singleton.shared[adx].count += 1;
        
        adx += 1
        
        performSegue(withIdentifier: "t", sender: nil)
    }
    
    
    @IBAction func _btnAtualizar(_ sender: Any) {
        
        performSegue(withIdentifier: "seguer2", sender: self)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

